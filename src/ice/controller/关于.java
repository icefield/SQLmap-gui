package ice.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 作者：icefield
 * 时间：2021/3/23
 * 描述：
 */
public class 关于 implements Initializable {
    @FXML
    private AnchorPane 背景;


    public 关于(){

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        BackgroundImage myBI= new BackgroundImage(new Image(getClass().getClassLoader().getResource("ice/resource/aboutbg.gif").toString()), BackgroundRepeat.ROUND, BackgroundRepeat.ROUND, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        背景.setBackground(new Background(myBI));

    }
}
