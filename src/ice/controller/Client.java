package ice.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import ice.util.HttpClient4;
import org.jsoup.Connection;

import java.io.IOException;

/*
 * 作者：icefield
 * 时间：2021/3/24
 * 描述：SQLmapapi客户端，把客户端的功能封装
 */
public class Client {
    private Connection con;
    private String taskid;
    private JSONObject jsonObject;
    private String body;
    private String url;
    private String server="http://127.0.0.1:8775";
    private JSONObject pdate;

    public Client(String url) {
        this.url=url;
    }
    public Client(String id,int a) {
        this.taskid=id;
    }
    public String 创建任务() throws IOException{
        body = HttpClient4.doGet("http://127.0.0.1:8775/task/new");
        jsonObject = JSONObject.parseObject(body);
        if (jsonObject.getBoolean("success")) {
            taskid=jsonObject.getString("taskid");
            return taskid;
        }
        return "创建错误";
    }
    public Boolean 开始任务() throws IOException {
        JSONObject pdate = new JSONObject();
        pdate.put("url",url);

        body = HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
//        System.out.println(body);
        jsonObject= JSONObject.parseObject(body);
        return jsonObject.getBoolean("success");
                
    }
    public Boolean 开始任务(JSONObject js) throws IOException {

        body = HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", js.toJSONString());
//        System.out.println(body);
        jsonObject= JSONObject.parseObject(body);
        return jsonObject.getBoolean("success");

    }
    public String 任务状态() throws IOException {
        body = HttpClient4.doGet(server+"/scan/"+taskid+"/status");
        jsonObject = JSONObject.parseObject(body);
        return jsonObject.getString("status");

    }
    public JSONArray 任务数据() throws IOException {

        body = HttpClient4.doGet(server+"/scan/" + taskid + "/data");
        jsonObject = JSONObject.parseObject(body);
        return jsonObject.getJSONArray("data");
    }

    public void 删除任务() throws IOException {
        HttpClient4.doGet(server+"/task/"+taskid+"/delete");
    }
    public void 日志获取次数() throws IOException {
        body = HttpClient4.doGet(server+"/scan/"+taskid+"/log" );
        jsonObject = JSONObject.parseObject(body);

    }
    public Boolean 结束任务() throws IOException {
        body = HttpClient4.doGet(server+"/scan/"+taskid+"/stop" );
        jsonObject = JSONObject.parseObject(body);
        return jsonObject.getBoolean("success");
    }
    public Boolean 杀死任务() throws IOException {
        body = HttpClient4.doGet(server+"/scan/"+taskid+"/kill" );
        jsonObject = JSONObject.parseObject(body);
        return jsonObject.getBoolean("success");
    }
    public JSONArray 开始查询() throws Exception {
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("getBanner",true);
        pdate.put("getDbs",true);
        pdate.put("getCurrentDb",true);
        pdate.put("getCurrentUser",true);
        pdate.put("getUsers",true);
        pdate.put("extensiveFp",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        return 任务数据;
    }
    public JSONArray 获取表名() throws Exception {
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("getTables",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        return 任务数据;


    }

    public JSONArray 获取段名(String db,String tb) throws Exception {
        String tmp=taskid;
        创建任务();
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("db",db);
        pdate.put("tbl",tb);
        pdate.put("getColumns",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());

        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        删除任务();
        taskid=tmp;
        return 任务数据;
    }

    public JSONArray dump表(String db,String tb) throws Exception {
        String tmp=taskid;
        创建任务();
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("db",db);
        pdate.put("tbl",tb);
        pdate.put("dumpTable",true);
        pdate.put("charset","UTF-8");
        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        删除任务();
        taskid=tmp;
        return 任务数据;
    }
    public JSONArray dump段(String db,String tb,String co) throws Exception {
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("db",db);
        pdate.put("tbl",tb);
        pdate.put("col",co);
        pdate.put("dumpTable",true);
        pdate.put("charset","UTF-8");
        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        return 任务数据;
    }

    public JSONArray 获取记录数(String db,String tb) throws Exception {
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("db",db);
        pdate.put("tbl",tb);
        pdate.put("getCount",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());
        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        return 任务数据;
    }

    public JSONArray 获取密码() throws Exception {
        String tmp=taskid;
        创建任务();
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("getPasswordHashes",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());

        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(600);
        }
        JSONArray 任务数据 = 任务数据();
        删除任务();
        taskid=tmp;
        return 任务数据;
    }

    public JSONArray 获取权限() throws Exception {
        String tmp=taskid;
        创建任务();
        pdate = new JSONObject();
        pdate.put("url",url);
        pdate.put("getPrivileges",true);
        pdate.put("charset","UTF-8");

        HttpClient4.doPost("http://127.0.0.1:8775/scan/" + taskid + "/start", pdate.toJSONString());

        boolean flag=true;
        /*
         *  作者：icefield
         *  说明：检测扫描是否结束，结束则执行 任务数据();
         */
        while (flag){
            flag=!任务状态().equals("terminated");
            Thread.sleep(300);
        }
        JSONArray 任务数据 = 任务数据();
        删除任务();
        taskid=tmp;
        return 任务数据;
    }
}
