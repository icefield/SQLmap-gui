package ice.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

/*
 * 作者：icefield
 * 时间：2021/3/22
 * 描述：
 */
public class MainStage implements Initializable {
    @FXML
    private BorderPane 主面板;
    @FXML
    private Menu 单一;

    @FXML
    private Menu 批量;

    @FXML
    private Menu 高级;

    @FXML
    private Menu 关于;

    AnchorPane 单一面板;
    AnchorPane 批量面板;
    AnchorPane 高级面板;
    AnchorPane 关于面板;
    public static  MainStage  mstage;

    public MainStage() {
        mstage=this;
    }

    /*
     *  作者：icefield
     *  说明：初始化主面板
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Label lb1 = new Label("单一注入测试");
        Label lb2 = new Label("批量注入测试");
        Label lb3 = new Label("高级注入测试");
        Label lb4 = new Label("关于");
        lb1.setOnMouseClicked(event -> {单击单一();});
        lb2.setOnMouseClicked(event -> {单击批量();});
        lb3.setOnMouseClicked(event -> {单击高级();});
        lb4.setOnMouseClicked(event -> {单击关于();});

        单一.setGraphic(lb1);
        批量.setGraphic(lb2);
        高级.setGraphic(lb3);
        关于.setGraphic(lb4);

        try {
            单一面板 = (AnchorPane) FXMLLoader.load(getClass().getResource("/ice/fxml/danyi.fxml"));
            批量面板 = (AnchorPane) FXMLLoader.load(getClass().getResource("/ice/fxml/piliang.fxml"));
            高级面板 = (AnchorPane) FXMLLoader.load(getClass().getResource("/ice/fxml/gaoji.fxml"));
            关于面板 = (AnchorPane) FXMLLoader.load(getClass().getResource("/ice/fxml/about.fxml"));
            主面板.setCenter(单一面板);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void 单击单一() {
        主面板.setCenter(单一面板);
    }

    public void 单击批量() {
        主面板.setCenter(批量面板);
    }

    public void 单击高级() {
        主面板.setCenter(高级面板);
    }

    public void 单击关于() {
        主面板.setCenter(关于面板);
    }
}
