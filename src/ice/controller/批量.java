package ice.controller;

import com.alibaba.fastjson.JSONObject;
import ice.util.tools;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/*
 * 作者：icefield
 * 时间：2021/3/22
 * 描述：批量测试模块功能代码
 */
public class 批量 implements Initializable {

    @FXML
    private Button 按钮批量开始;

    @FXML
    private TextArea 目标框;

    @FXML
    private TextArea 结果框;

    @FXML
    private Button 按钮清空下;
    @FXML
    private Button 按钮清空上;

    @FXML
    private AnchorPane 背景1;

    public 批量(){
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BackgroundImage bg1 = new BackgroundImage(new Image(getClass().getResourceAsStream("/ice/resource/冬.jpg")), BackgroundRepeat.ROUND, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true));

        背景1.setBackground(new Background(bg1));

        目标框.setStyle("-fx-background-color:rgba(249,252,252,0.8);-fx-text-fill:#290030 ");
        结果框.setStyle("-fx-background-color:rgba(249,252,252,0.8);-fx-text-fill:#290030 ");
    }

    public TextArea get目标框() {
        return 目标框;
    }

    public TextArea get结果框() {
        return 结果框;
    }

    @FXML
    void 开始批量(MouseEvent event) {
        按钮批量开始.setDisable(true);
        String urls = 目标框.getText();
        if (urls.trim().equals("")) {
            tools.提醒弹窗("请输入目标URL");
            按钮批量开始.setDisable(false);
            return;
        }
        new Thread(() -> {
            try {
                ArrayList<String> ulist = geturl(urls);
                String result="";
                JSONObject data = new JSONObject();
                //'batch':True,'randomAgent':True,'tamper':'space2comment','tech':'BT','timeout':15,'level':1
                data.put("batch",true);
                data.put("randomAgent",true);
                data.put("tamper","space2comment");
                data.put("tech","BT");
                data.put("timeout",15);
                data.put("level",1);
                for (String u : ulist) {
                    tools.updataStatus("****************************************************************************************\r\n", 结果框);
                    tools.updataStatus("【"+u+"】检测开始\r\n", 结果框);
                    data.put("url",u);
                    Client c = new Client(u);
                    String 创建任务 = c.创建任务();
                    if (创建任务.equals("创建错误")){
                        tools.updataStatus("【"+u+"】创建失败\r\n", 结果框);
                        break;
                    }else{
                        tools.updataStatus("【"+u+"】任务创建成功，id："+创建任务+"\r\n", 结果框);
                        Boolean 开始任务 = c.开始任务(data);
                        if (开始任务){
                            tools.updataStatus("【"+u+"】开始测试任务\r\n", 结果框);
                        }else {
                            tools.updataStatus("【"+u+"】任务开始失败\r\n", 结果框);
                        }
                        String 任务状态;
                        String resdata;
                        long startTime = System.currentTimeMillis();
                        while (开始任务) {
                            任务状态 = c.任务状态();
//                            System.out.println(任务状态);
                            if (任务状态.equals("running")) {
                                tools.updataStatus("#", 结果框);
                            } else if (任务状态.equals("terminated")) {
                                tools.updataStatus("\r\n【"+u+"】扫描完成！\r\n", 结果框);
                                resdata = c.任务数据().toJSONString();
                                if (resdata.length()>2) {
                                    tools.updataStatus("【"+u+"】存在注入漏洞\r\n", 结果框);
                                    result+=u+"\r\n";
                                }else{
                                    tools.updataStatus("【"+u+"】无法注入\r\n", 结果框);
                                    c.删除任务();
                                }
                                break;
                            } else {
                                tools.updataStatus("\r\n系统异常\r\n", 结果框);
                                break;
                            }
                            Thread.sleep(1000);
//                            System.out.println(System.currentTimeMillis()-startTime);
                            if (System.currentTimeMillis()-startTime>30000){
                                tools.updataStatus("\r\n【"+u+"】扫描超时\r\n", 结果框);
                                if(c.结束任务()){
                                    tools.updataStatus("【"+u+"】任务已结束\r\n", 结果框);
                                }else{
                                    tools.updataStatus("【"+u+"】无法结束任务\r\n", 结果框);
                                }
                                if(c.杀死任务()){
                                    tools.updataStatus("【"+u+"】任务已杀死\r\n", 结果框);
                                }else{
                                    tools.updataStatus("【"+u+"】无法杀死任务\r\n", 结果框);
                                }
                                c.删除任务();
                                tools.updataStatus("【"+u+"】已删除任务\r\n", 结果框);
                                break;
                            }
                        }
                        c.删除任务();
                    }
                }
                tools.updataStatus("\r\n可注入的目标：\r\n"+result,结果框);
            } catch (Exception e) {
                e.printStackTrace();
                tools.updataStatus("系统异常\r\n",结果框);
            }
            按钮批量开始.setDisable(false);
        }).start();
    }

    @FXML
    void 清空下(MouseEvent event) {
        结果框.setText("");
    }
    @FXML
    void 清空上(MouseEvent event) {
        目标框.setText("");
    }

    private ArrayList<String> geturl(String urltxt){
        ArrayList<String> urllist = new ArrayList<>();
        try {
            StringReader sreader = new StringReader(urltxt);
            BufferedReader bf = new BufferedReader(sreader);
            String line="";
            tools.updataStatus("待检测URL:\r\n",结果框);
            while ((line = bf.readLine()) != null) {
                if (line.indexOf("?")!=-1){
                    urllist.add(line);
                    tools.updataStatus(line+"\r\n",结果框);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            tools.updataStatus("系统异常\r\n",结果框);
        }
        return urllist;
    }
}
