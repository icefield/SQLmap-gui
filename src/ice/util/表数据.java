package ice.util;

import java.util.ArrayList;

/*
 *   作者：icefield
 *   时间：2021/3/26
 *   描述：
 */
public class 表数据 {
    private String v0;
    private String v1;
    private String v2;
    private String v3;
    private String v4;
    private String v5;
    private String v6;
    private String v7;
    private String v8;
    private String v9;
    private String v10;
    private String v11;
    private String v12;
    private String v13;
    private String v14;
    private String v15;
    private String v16;
    private String v17;
    private String v18;
    private String v19;

    public 表数据(String v0) {
        this.v0 = v0;
    }

    public 表数据(String v0, String v1, String v2, String v3, String v4, String v5, String v6, String v7, String v8, String v9) {
        this.v0 = v0;
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.v4 = v4;
        this.v5 = v5;
        this.v6 = v6;
        this.v7 = v7;
        this.v8 = v8;
        this.v9 = v9;
    }

    @Override
    public String toString() {
        return "表数据{" +
                "v0='" + v0 + '\'' +
                ", v1='" + v1 + '\'' +
                ", v2='" + v2 + '\'' +
                ", v3='" + v3 + '\'' +
                ", v4='" + v4 + '\'' +
                ", v5='" + v5 + '\'' +
                ", v6='" + v6 + '\'' +
                ", v7='" + v7 + '\'' +
                ", v8='" + v8 + '\'' +
                ", v9='" + v9 + '\'' +
                '}';
    }

    public 表数据(ArrayList<String> str) {
        switch (str.size()){
            case 1:
                v0=str.get(0);
                break;
            case 2:
                v0=str.get(0);
                v1=str.get(1);
                break;
            case 3:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                break;
            case 4:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                break;
            case 5:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                break;
            case 6:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                break;
            case 7:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                break;
            case 8:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                break;
            case 9:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                break;
            case 10:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                break;
            case 11:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);

                break;
            case 12:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);

                break;
            case 13:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);

                break;
            case 14:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);

                break;
            case 15:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);

                break;
            case 16:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);
                v15=str.get(15);

                break;
            case 17:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);
                v15=str.get(15);
                v16=str.get(16);

                break;
            case 18:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);
                v15=str.get(15);
                v16=str.get(16);
                v17=str.get(17);

                break;
            case 19:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);
                v15=str.get(15);
                v16=str.get(16);
                v17=str.get(17);
                v18=str.get(18);
                break;
            case 20:
                v0=str.get(0);
                v1=str.get(1);
                v2=str.get(2);
                v3=str.get(3);
                v4=str.get(4);
                v5=str.get(5);
                v6=str.get(6);
                v7=str.get(7);
                v8=str.get(8);
                v9=str.get(9);
                v10=str.get(10);
                v11=str.get(11);
                v12=str.get(12);
                v13=str.get(13);
                v14=str.get(14);
                v15=str.get(15);
                v16=str.get(16);
                v17=str.get(17);
                v18=str.get(18);
                v19=str.get(19);
                break;
            default:
        }
    }

    public String getV0() {
        return v0;
    }

    public void setV0(String v0) {
        this.v0 = v0;
    }

    public String getV1() {
        return v1;
    }

    public void setV1(String v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }

    public String getV3() {
        return v3;
    }

    public void setV3(String v3) {
        this.v3 = v3;
    }

    public String getV4() {
        return v4;
    }

    public void setV4(String v4) {
        this.v4 = v4;
    }

    public String getV5() {
        return v5;
    }

    public void setV5(String v5) {
        this.v5 = v5;
    }

    public String getV6() {
        return v6;
    }

    public void setV6(String v6) {
        this.v6 = v6;
    }

    public String getV7() {
        return v7;
    }

    public void setV7(String v7) {
        this.v7 = v7;
    }

    public String getV8() {
        return v8;
    }

    public void setV8(String v8) {
        this.v8 = v8;
    }

    public String getV9() {
        return v9;
    }

    public void setV9(String v9) {
        this.v9 = v9;
    }
}
