# 基于SQLmap的SQL注入工具

#### 介绍
基于SQLmap开发的SQL注入工具

#### 软件架构
基于SQLmap，使用Java开发


#### 安装教程
安装JDK（需要有javafx）
安装Python
安装SQLmap

#### 演示
![image](https://gitee.com/icefield/SQLmap-gui/raw/master/%E6%BC%94%E7%A4%BA.gif)
